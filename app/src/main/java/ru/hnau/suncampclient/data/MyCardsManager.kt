package ru.hnau.suncampclient.data

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.getter.Getter
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.async.GetterAsyncDataProducer
import ru.hnau.suncamp.common.data.Card
import ru.hnau.suncampclient.api.Api


object MyCardsManager : SunCampDataManager<Possible<List<Card>>>(
    { Api.cardGetList() }
)