package ru.hnau.suncampclient.data

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.finisher.mapPossible
import ru.hnau.jutils.getter.Getter
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.async.GetterAsyncDataProducer
import ru.hnau.suncamp.common.api.response.SubordinateStorageInfo
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.api.Api


object SubordinateStoragesManager : SunCampDataManager<Possible<List<SubordinateStorageInfo>>>(
        {
            Api.storageManagerGetSubordinateList().mapPossible { storages ->
                storages.map { storage ->
                    SubordinateStorageInfo(
                            storageUserLogin = storage.storageUserLogin,
                            balance = storage.balance,
                            transactions = storage.transactions.sortedByDescending { it.timestamp }
                    )
                }
            }
        }
)