package ru.hnau.suncampclient.data

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.getter.Getter
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.async.AsyncDataProducer
import ru.hnau.jutils.producer.async.GetterAsyncDataProducer
import ru.hnau.suncampclient.api.Api
import ru.hnau.suncampclient.utils.LoginManager


object BalanceManager : SunCampDataManager<Possible<Long>>(
        { Api.balanceGet() }
)