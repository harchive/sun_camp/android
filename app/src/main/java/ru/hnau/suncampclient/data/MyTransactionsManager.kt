package ru.hnau.suncampclient.data

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.getter.Getter
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.async.AsyncDataProducer
import ru.hnau.jutils.producer.async.GetterAsyncDataProducer
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncamp.common.data.TransactionCategory
import ru.hnau.suncampclient.api.Api


object MyTransactionsManager : SunCampDataManager<Possible<List<Transaction>>>(
        { Api.transactionGetList() }
)