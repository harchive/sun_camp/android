package ru.hnau.suncampclient.pages.transaction_info

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.LinearLayoutSeparator
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.finisher.awaitSuccess
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.jutils.takeIfNotBlank
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.pages.new_transaction.NewTransactionPage
import ru.hnau.suncampclient.pages.new_transaction.param.NewTransactionParam
import ru.hnau.suncampclient.pages.new_transaction.param.NewUserToUserTransactionParam
import ru.hnau.suncampclient.ui.view.SunFrameLayout
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.*
import ru.hnau.suncampclient.utils.timestamp.TimestampShowingType


@SuppressLint("ViewConstructor")
class TransactionInfoPageInnerView(
        context: Context,
        private val transaction: Transaction
) : FrameLayout(
        context
) {

    companion object {

        private val KEY_LABEL_INFO = LabelInfo(
                textSize = DpPxGetter.fromDp(20),
                fontType = FontManager.PT_SANS,
                maxLines = 1,
                minLines = 1,
                gravity = HGravity.LEFT_CENTER_VERTICAL,
                textColor = ColorManager.BROWN
        )

        private val VALUE_LABEL_INFO = KEY_LABEL_INFO.copy(
                textColor = ColorManager.ORANGE,
                gravity = HGravity.RIGHT_CENTER_VERTICAL
        )

    }

    private val finishersLockedProducer = FinishersLockedProducer()

    private val waiterView = ColorManager.createWaiter(context, finishersLockedProducer)

    private val titleView = Label(
            context = context,
            textColor = ColorManager.RED,
            gravity = HGravity.CENTER,
            textSize = DpPxGetter.fromDp(40),
            minLines = 1,
            maxLines = 1,
            fontType = FontManager.PT_SANS,
            initialText = StringGetter(R.string.transaction_info_page_title)
    )

    private val propertiesContainer = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL
        setPadding(DpPxGetter.ZERO, DpPxGetter.fromDp(16))

        UsersManager.currentData.await {
            val fromName = it.data?.find { it.login == transaction.fromUserLogin }?.name ?: ""
            val toName = it.data?.find { it.login == transaction.toUserLogin }?.name ?: ""
            val initiatorName = it.data?.find { it.login == transaction.initiatorUserLogin }?.name
                    ?: ""

            listOf(
                    R.string.transaction_info_page_key_from to fromName,
                    R.string.transaction_info_page_key_to to toName,
                    R.string.transaction_info_page_key_initiator to initiatorName,
                    R.string.transaction_info_page_key_date to TimestampShowingType.DATE.formatTimestamp(transaction.timestamp),
                    R.string.transaction_info_page_key_time to TimestampShowingType.TIME.formatTimestamp(transaction.timestamp),
                    R.string.transaction_info_page_key_type to transaction.category.title
            ).forEach {

                val keyView = Label(
                        context = context,
                        info = KEY_LABEL_INFO,
                        initialText = StringGetter(it.first)
                ).apply {
                    setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f) {
                        setRightMargin(SizeManager.DEFAULT_PADDING.get(context))
                    }
                }

                val valueView = Label(
                        context = context,
                        info = VALUE_LABEL_INFO,
                        initialText = it.second.toGetter()
                ).apply {
                    setLinearLayoutLayoutParams(0, WRAP_CONTENT, 3f)
                }

                val propertyView = LinearLayout(context).apply {
                    orientation = LinearLayout.HORIZONTAL
                    setPadding(DpPxGetter.ZERO, DpPxGetter.fromDp(4))
                    addView(keyView)
                    addView(valueView)
                }

                addView(propertyView)

            }

        }

    }

    private val commentViews = transaction.comment.takeIfNotBlank()?.let { comment ->
        listOf(

                Label(
                        context = context,
                        initialText = StringGetter(R.string.transaction_info_page_key_comment),
                        info = KEY_LABEL_INFO
                ).apply {
                    setBottomPadding(DpPxGetter.fromDp(4))
                },

                Label(
                        context = context,
                        initialText = StringGetter(comment),
                        info = KEY_LABEL_INFO.copy(
                                textColor = ColorManager.ORANGE,
                                maxLines = 3
                        )
                ).apply {
                    setBottomPadding(DpPxGetter.fromDp(8))
                }

        )
    }

    private val totalAmountView = Label(
            context = context,
            initialText = StringGetter(R.string.transaction_info_page_amount_title) + ": ${transaction.amount}".toGetter(),
            textSize = DpPxGetter.fromDp(32),
            fontType = FontManager.PT_SANS,
            maxLines = 1,
            minLines = 1,
            gravity = HGravity.RIGHT,
            textColor = ColorManager.RED
    ).apply {
        setVerticalPadding(SizeManager.DEFAULT_PADDING)
    }

    private val receiptView = SunFrameLayout(context).apply {

        addView(LinearLayout(context).apply {
            orientation = LinearLayout.VERTICAL
            addView(titleView)
            addView(propertiesContainer)
            commentViews?.forEach(this::addView)
            addView(totalAmountView)
        })
    }.apply {

        setPadding(SizeManager.DEFAULT_PADDING * 2, SizeManager.DEFAULT_PADDING)

        setLinearLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT) {
            gravity = Gravity.TOP
            setVerticalMargins(SizeManager.DEFAULT_PADDING.get(context))
        }
    }

    private val repeatButton = if (transaction.fromUserLogin != LoginManager.login) null else {
        SunButton(
                context = context,
                color = SunButtonColorInfo.BROWN_ON_YELLOW,
                size = SunButtonSizeInfo.SIZE_24,
                shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
                text = StringGetter(R.string.transaction_info_page_button_repeat),
                onClick = this::onRepeatButtonClicked
        )
    }

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL

        addView(receiptView)
        addView(LinearLayoutSeparator(context))
        repeatButton?.let(this::addView)
    }

    init {
        addView(contentContainer)
        addView(waiterView)
    }

    private fun onRepeatButtonClicked() {
        NewUserToUserTransactionParam.forTransaction(transaction)
                .addToLocker(finishersLockedProducer)
                .awaitSuccess { newTransactionParam ->
                    AppActivityConnector.showPage(NewTransactionPage(newTransactionParam))
                }
    }


}