package ru.hnau.suncampclient.pages.choose_outcome_user

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.header.PageViewWithHeader
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class ChooseOutcomeUserPageView(
        context: Context,
        onBackClicked: () -> Unit
) : PageViewWithHeader<ChooseOutcomeUserPageInnerView>(
        context = context,
        title = StringGetter(R.string.choose_outcome_user_page_title),
        rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
        onBackClicked = onBackClicked,
        contentView = ChooseOutcomeUserPageInnerView(context),
        headerInfo = ColorManager.HEADER_INFO,
        headerBackButtonInfo = ColorManager.HEADER_BACK_BUTTON_INFO,
        headerLabelInfo = ColorManager.HEADER_LABEL_INFO
)