package ru.hnau.suncampclient.pages.new_transaction.param

import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.finisher.mapPossible
import ru.hnau.jutils.finisher.mapPossibleOrError
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncamp.common.data.TransactionCategory
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.api.Api
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.pages.new_transaction.TransactionOrNull


class NewUserToUserTransactionParam(
        val fromUser: User,
        val toUser: User,
        amount: Long? = null,
        category: TransactionCategory = TransactionCategory.payment,
        comment: String = ""
) : NewTransactionParam(
        amount,
        category,
        comment
) {

    companion object {

        fun forTransaction(transaction: Transaction) =
                UsersManager.currentData.mapPossibleOrError { users ->
                    val fromUser = users.find { it.login == transaction.fromUserLogin }
                    val toUser = users.find { it.login == transaction.toUserLogin }
                    if (fromUser == null || toUser == null) {
                        return@mapPossibleOrError Possible.error<NewUserToUserTransactionParam>()
                    }
                    Possible.success(
                            NewUserToUserTransactionParam(
                                    fromUser = fromUser,
                                    toUser = toUser,
                                    amount = transaction.amount,
                                    category = transaction.category,
                                    comment = transaction.comment
                            )
                    )
                }

    }

    override fun execute(amount: Long, category: TransactionCategory, comment: String) =
            Api.transactionExecute(
                    fromUserLogin = fromUser.login,
                    toUserLogin = toUser.login,
                    category = category,
                    amount = amount,
                    comment = comment
            ).mapPossible(::TransactionOrNull)

}