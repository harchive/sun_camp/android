package ru.hnau.suncampclient.pages.main.view.storages

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.suncamp.common.api.response.SubordinateStorageInfo
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.SunClickableFrameLayout
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager


@SuppressLint("ViewConstructor")
class SubordinateStoragesListItemView(
        context: Context,
        private val onStorageClicked: (Pair<SubordinateStorageInfo, User>) -> Unit
) : SunClickableFrameLayout(
        context = context,
        shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
        rippleDrawInfo = ColorManager.WHITE_RIPPLE_DRAW_INFO
), BaseListViewWrapper<Pair<SubordinateStorageInfo, User>> {

    override val view: View
        get() = this

    private var storage: Pair<SubordinateStorageInfo, User>? = null


    private val iconView = ImageView(context).apply {
        setImageResource(R.drawable.ic_storage)
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
    }

    private val nameView = Label(
            context = context,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(20),
            gravity = HGravity.LEFT_CENTER_VERTICAL,
            fontType = FontManager.PT_SANS,
            textColor = ColorManager.BROWN
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f) {
            setHorizontalMargins(SizeManager.DEFAULT_PADDING.get(context))
        }
    }

    private val balanceView = Label(
            context = context,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(20),
            gravity = HGravity.RIGHT_CENTER_VERTICAL,
            fontType = FontManager.PT_SANS,
            textColor = ColorManager.ORANGE
    )

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER
        addView(iconView)
        addView(nameView)
        addView(balanceView)
    }

    init {
        addView(contentContainer)
        setPadding(SizeManager.DEFAULT_PADDING, SizeManager.DEFAULT_PADDING)
    }

    override fun setContent(content: Pair<SubordinateStorageInfo, User>) {
        this.storage = content
        nameView.text = StringGetter(content.second.name)
        balanceView.text = content.first.balance.toString().toGetter()
    }

    override fun onClicked() {
        super.onClicked()
        val storage = this.storage ?: return
        onStorageClicked.invoke(storage)
    }

}