package ru.hnau.suncampclient.pages.new_transaction

import android.annotation.SuppressLint
import android.content.Context
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.LinearLayoutSeparator
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.utils.ToastDuration
import ru.hnau.androidutils.utils.showToast
import ru.hnau.jutils.finisher.awaitSuccess
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.jutils.takeIfPositive
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.BalanceManager
import ru.hnau.suncampclient.data.MyTransactionsManager
import ru.hnau.suncampclient.data.SubordinateStoragesManager
import ru.hnau.suncampclient.pages.main.MainPage
import ru.hnau.suncampclient.pages.new_transaction.param.NewStorageToUsersTransactionParam
import ru.hnau.suncampclient.pages.new_transaction.param.NewTransactionParam
import ru.hnau.suncampclient.pages.new_transaction.param.NewUserToUserTransactionParam
import ru.hnau.suncampclient.pages.new_transaction.param.NewUsersToStorageTransactionParam
import ru.hnau.suncampclient.pages.transaction_info.TransactionInfoPage
import ru.hnau.suncampclient.ui.view.input.Input
import ru.hnau.suncampclient.ui.view.input.InputImeAction
import ru.hnau.suncampclient.ui.view.input.InputSymbolsType
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.SizeManager
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager


@SuppressLint("ViewConstructor")
class NewTransactionPageInnerView(
        context: Context,
        private val param: NewTransactionParam
) : FrameLayout(
        context
) {

    private val fromView = when (param) {
        is NewUserToUserTransactionParam -> NewTransactionPageUserOrUsersView(context, param.fromUser)
        is NewStorageToUsersTransactionParam -> NewTransactionPageUserOrUsersView(context, param.fromUser)
        else -> NewTransactionPageUserOrUsersView(context, (param as NewUsersToStorageTransactionParam).fromUsers)
    }

    private val toView = when (param) {
        is NewUserToUserTransactionParam -> NewTransactionPageUserOrUsersView(context, param.toUser)
        is NewUsersToStorageTransactionParam -> NewTransactionPageUserOrUsersView(context, param.toUser)
        else -> NewTransactionPageUserOrUsersView(context, (param as NewStorageToUsersTransactionParam).toUsers)
    }

    private val fromToSeparator = ImageView(context).apply {
        setImageResource(R.drawable.ic_arrow_down)
        setBottomPadding(DpPxGetter.fromDp(8))
    }

    private val commentInput = Input(
            context = context,
            shadowInfo = ColorManager.SHADOW_INFO_NORMAL,
            info = ColorManager.INPUT_INFO.copy(
                    symbolsType = InputSymbolsType.TEXT,
                    imeAction = InputImeAction.NEXT,
                    textColor = ColorManager.ORANGE
            ),
            initialText = param.comment,
            hintText = StringGetter(R.string.new_transaction_page_comment_hint)
    )

    private val amountInput = Input(
            context = context,
            shadowInfo = ColorManager.SHADOW_INFO_NORMAL,
            info = ColorManager.INPUT_INFO.copy(
                    maxLength = 5,
                    symbolsType = InputSymbolsType.NUMBERS,
                    imeAction = InputImeAction.DONE
            ),
            initialText = param.amount?.toString() ?: "",
            hintText = StringGetter(R.string.new_transaction_page_amount_hint)
    )

    private val payButton = SunButton(
            context = context,
            onClick = this::onPayButtonClicked,
            color = SunButtonColorInfo.BROWN_ON_YELLOW,
            text = StringGetter(R.string.new_transaction_page_pay_button),
            size = SunButtonSizeInfo.SIZE_24,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO
    ).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT) {
            setTopMargin(SizeManager.DEFAULT_PADDING.get(context))
        }
    }

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL
        setPadding(SizeManager.DEFAULT_PADDING)

        addView(fromView)
        addView(fromToSeparator)
        addView(toView)
        addView(LinearLayoutSeparator(context))
        addView(commentInput)
        addView(amountInput)
        addView(payButton)
    }

    private val scrollView = ScrollView(context).apply {
        isFillViewport = true
        addView(contentContainer)
    }

    private val finishersLockedProducer = FinishersLockedProducer()

    private val waiterView = ColorManager.createWaiter(context, finishersLockedProducer)

    init {
        addView(scrollView)
        addView(waiterView)
    }

    private fun onPayButtonClicked() {
        val amount = amountInput.text.toString().toLongOrNull()?.takeIfPositive()
        if (amount == null) {
            showToast(StringGetter(R.string.new_transaction_page_amount_error_incorrect), ToastDuration.LONG)
            return
        }

        param.execute(
                amount = amount,
                category = param.category,
                comment = commentInput.text.toString()
        )
                .addToLocker(finishersLockedProducer)
                .awaitSuccess { transactionOrNull ->

                    val transaction = transactionOrNull.transaction
                    if (transaction == null) {

                        SubordinateStoragesManager.invalidate()
                        DialogManager.show<Unit> {

                            title(StringGetter(R.string.new_transaction_page_success_dialog_title))

                            buttons {
                                button(
                                        text = StringGetter(R.string.new_transaction_page_success_dialog_button_close),
                                        onClick = { Possible.error() }
                                )
                            }

                        }.await {
                            AppActivityConnector.showPage(MainPage(), true)
                        }
                        return@awaitSuccess
                    }

                    MyTransactionsManager.invalidate()
                    BalanceManager.invalidate()

                    DialogManager.show<Unit> {

                        title(StringGetter(R.string.new_transaction_page_success_dialog_title))

                        buttons {
                            button(
                                    text = StringGetter(R.string.new_transaction_page_success_dialog_button_close),
                                    onClick = { Possible.error() }
                            )
                            button(
                                    text = StringGetter(R.string.new_transaction_page_success_dialog_button_info),
                                    onClick = { Possible.success(Unit) }
                            )
                        }

                    }.await {
                        it.handle(
                                onSuccess = {
                                    AppActivityConnector.showPage(
                                            page = TransactionInfoPage(transaction),
                                            clearStack = true,
                                            precedingPages = listOf(MainPage())
                                    )
                                },
                                onError = {
                                    AppActivityConnector.showPage(MainPage(), true)
                                }
                        )
                    }
                }
    }

}