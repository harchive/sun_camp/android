package ru.hnau.suncampclient.pages.choose_users

import android.annotation.SuppressLint
import android.content.Context
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.AutoSwipeRefreshView
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.UsersBalanceManager
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.pages.choose_users.list.ChooseUsersPageUsersListContainerView
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class ChooseUsersPageInnerView(
        context: Context,
        private val onUsersWasChosen: (Set<User>, ChooseUsersPageConnector) -> Unit,
        private val showPage: (page: Page<*>, clearStack: Boolean, precedingPages: List<Page<*>>) -> Boolean,
        private val goBack: (pageResolver: (List<Page<*>>) -> Int?) -> Boolean
) : AutoSwipeRefreshView(
        context = context,
        color = ColorManager.RED
), ChooseUsersPageConnector {

    override fun showPage(page: Page<*>, clearStack: Boolean, precedingPages: List<Page<*>>) =
            showPage.invoke(page, clearStack, precedingPages)

    override fun goBack(pageResolver: (List<Page<*>>) -> Int?) =
            goBack.invoke(pageResolver)

    override val finishersLockedProducer = FinishersLockedProducer()

    private val waiterView = ColorManager.createWaiter(context, finishersLockedProducer)

    private val usersListView = ChooseUsersPageUsersListContainerView(context).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f)
    }

    private val selectAllButton = SunButton(
            context = context,
            color = SunButtonColorInfo.BROWN_ON_WHITE,
            onClick = usersListView::selectAll,
            text = StringGetter(R.string.choose_users_page_select_all_button),
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
            size = SunButtonSizeInfo.SIZE_16
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f)
    }

    private val unselectAllButton = SunButton(
            context = context,
            color = SunButtonColorInfo.BROWN_ON_WHITE,
            onClick = usersListView::unselectAll,
            text = StringGetter(R.string.choose_users_page_unselect_all_button),
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
            size = SunButtonSizeInfo.SIZE_16
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f)
    }

    private val allButtonsContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        addView(selectAllButton)
        addView(unselectAllButton)
    }

    private val nextButton = SunButton(
            context = context,
            color = SunButtonColorInfo.BROWN_ON_YELLOW,
            onClick = this::onNextButtonClicked,
            text = StringGetter(R.string.choose_users_page_next_button),
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
            size = SunButtonSizeInfo.SIZE_24
    )

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL
        addView(usersListView)
        addView(allButtonsContainer)
        addView(nextButton)
    }

    init {
        addView(contentContainer)
        addView(waiterView)
    }

    override fun updateContent() {
        UsersManager.invalidate()
        UsersBalanceManager.invalidate()
    }

    private fun onNextButtonClicked() {
        val selectedUsers = usersListView.selectedUsers
        onUsersWasChosen.invoke(selectedUsers, this)
    }

}