package ru.hnau.suncampclient.pages.new_transaction

import ru.hnau.suncamp.common.data.Transaction


data class TransactionOrNull(
        val transaction: Transaction? = null
)