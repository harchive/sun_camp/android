package ru.hnau.suncampclient.pages.choose_users

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncamp.common.data.User


class ChooseUsersPage(
        private val title: StringGetter,
        private val onUsersWasChosen: (Set<User>, ChooseUsersPageConnector) -> Unit
) : Page<ChooseUsersPageView>() {

    override fun generateView(context: Context) =
            ChooseUsersPageView(
                    context = context,
                    title = title,
                    onUsersWasChosen = onUsersWasChosen,
                    showPage = this::showPage,
                    goBack = this::goBack,
                    onBackClicked = this::onBackClicked
            )

    private fun onBackClicked() {
        goBack()
    }
}