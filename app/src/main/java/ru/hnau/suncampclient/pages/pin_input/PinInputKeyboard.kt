package ru.hnau.suncampclient.pages.pin_input

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawableLayoutType
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.LinearLayoutSeparator
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButtonSize
import ru.hnau.androidutils.ui.view.buttons.circle.icon.CircleIconButton
import ru.hnau.androidutils.ui.view.buttons.circle.text.CircleTextButton
import ru.hnau.androidutils.ui.view.buttons.circle.text.CircleTextButtonInfo
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager


@SuppressLint("ViewConstructor")
class PinInputKeyboard(
        context: Context,
        private val onNumberClicked: (number: Int) -> Unit,
        private val onBackspaceClicked: () -> Unit
) : LinearLayout(context) {

    companion object {

        private val CIRCLE_BUTTON_SIZE = CircleButtonSize(DpPxGetter.fromDp(64))

        private val CIRCLE_TEXT_BUTTON_INFO = CircleTextButtonInfo(
                textSize = DpPxGetter.fromDp(40),
                fontType = FontManager.PT_SANS,
                textColor = ColorManager.BROWN
        )

    }

    init {
        orientation = VERTICAL

        var lineContainer: ViewGroup? = null
        (1..12).forEachIndexed { i, num ->

            if (i % 3 == 0) {
                lineContainer?.let(this::addView)
                lineContainer = LinearLayout(context).apply {
                    orientation = HORIZONTAL
                    setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f)
                }
            }

            val buttonView = when (num) {
                10 -> LinearLayoutSeparator(context)
                11 -> getNumberButton(0)
                12 -> getBackspaceButton()
                else -> getNumberButton(num)
            }

            buttonView.setLinearLayoutLayoutParams(0, MATCH_PARENT, 1f)
            lineContainer?.addView(buttonView)


        }

        lineContainer?.let(this::addView)
    }

    private fun getNumberButton(number: Int) = CircleTextButton(
            context = context,
            onClick = { onNumberClicked.invoke(number) },
            text = number.toString().toGetter(),
            rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
            size = CIRCLE_BUTTON_SIZE,
            info = CIRCLE_TEXT_BUTTON_INFO,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO
    )

    private fun getBackspaceButton() = CircleIconButton(
            context = context,
            onClick = onBackspaceClicked,
            icon = LayoutDrawable(
                    context = context,
                    gravity = HGravity.CENTER,
                    layoutType = LayoutDrawableLayoutType.PROPORTIONAL_IN,
                    content = DrawableGetter(R.drawable.ic_backspace)
            ).toGetter(),
            rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
            size = CIRCLE_BUTTON_SIZE,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO
    )

}