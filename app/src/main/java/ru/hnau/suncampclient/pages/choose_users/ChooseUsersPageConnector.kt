package ru.hnau.suncampclient.pages.choose_users

import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.androidutils.ui.view.pager.PagePagerConnector
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer


interface ChooseUsersPageConnector {

    val finishersLockedProducer: FinishersLockedProducer

    fun showPage(
            page: Page<*>,
            clearStack: Boolean = false,
            precedingPages: List<Page<*>> = emptyList()
    ): Boolean

    fun goBack(
            pageResolver: (List<Page<*>>) -> Int? = PagePagerConnector.DEFAULT_GO_BACK_PAGE_RESOLVER
    ): Boolean

}