package ru.hnau.suncampclient.pages.main.view.tabs

import android.content.Context
import android.graphics.*
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.view.MotionEvent
import ru.hnau.androidutils.animations.TwoStatesAnimator
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.ripple.RippleDrawer
import ru.hnau.androidutils.ui.ripple.RippleDrawerRectFShapeInfo
import ru.hnau.androidutils.ui.utils.drawing.ColorUtils
import ru.hnau.jutils.TimeValue
import ru.hnau.suncampclient.utils.ColorManager


class TabButton(
        context: Context,
        private val pos: Int,
        tab: Tab,
        private val onNeedRefresh: () -> Unit,
        private val onClicked: () -> Unit
) {

    companion object {

        private val INACTIVE_COLOR = ColorManager.WHITE
        private val ACTIVE_COLOR = ColorManager.YELLOW

        private val INACTIVE_TITLE_TEXT_SIZE = DpPxGetter.fromDp(12)
        private val ACTIVE_TITLE_TEXT_SIZE = DpPxGetter.fromDp(14)

        private val INACTIVE_ICON_OFFSET_Y = DpPxGetter.fromDp(4)
        private val ACTIVE_ICON_OFFSET_Y = DpPxGetter.fromDp(2)

        private val INACTIVE_TITLE_OFFSET_Y = DpPxGetter.fromDp(16)
        private val ACTIVE_TITLE_OFFSET_Y = DpPxGetter.fromDp(16)

    }

    private val inactiveColor = INACTIVE_COLOR.get(context)
    private val activeColor = ACTIVE_COLOR.get(context)

    private val inactiveTitleTextSize = INACTIVE_TITLE_TEXT_SIZE.getPx(context)
    private val activeTitleTextSize = ACTIVE_TITLE_TEXT_SIZE.getPx(context)

    private val inactiveIconOffsetY = INACTIVE_ICON_OFFSET_Y.getPx(context)
    private val activeIconOffsetY = ACTIVE_ICON_OFFSET_Y.getPx(context)

    private val inactiveTitleOffsetY = INACTIVE_TITLE_OFFSET_Y.getPx(context)
    private val activeTitleOffsetY = ACTIVE_TITLE_OFFSET_Y.getPx(context)

    private val bounds = RectF()

    private val rippleDrawer = RippleDrawer(
            context = context,
            rippleDrawInfo = ColorManager.BROWN_YELLOW_RIPPLE_DRAW_INFO,
            shapeInfo = RippleDrawerRectFShapeInfo(bounds)
    )

    private val icon = tab.icon.get(context)

    private val iconWrap = DrawableCompat.wrap(icon)

    private val iconSize = Point(icon.intrinsicWidth, icon.intrinsicHeight)

    private val title = tab.title.get(context)

    private val titlePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        textAlign = Paint.Align.CENTER
    }

    private val titleDrawPoint = PointF()

    private var color = Color.TRANSPARENT
        set(value) {
            if (field != value) {
                field = value
                titlePaint.color = value
                DrawableCompat.setTint(iconWrap, value)
            }
        }

    private val selectedAnimator = TwoStatesAnimator(
            switchingTime = TimeValue.MILLISECOND * 100,
            interpolator = FastOutSlowInInterpolator()
    )

    private var selectedPercentage = 0f
        set(value) {
            if (field != value) {
                field = value
                updateDrawInfo()
                onNeedRefresh.invoke()
            }
        }

    private fun updateDrawInfo() {

        titlePaint.textSize = inactiveTitleTextSize + (activeTitleTextSize - inactiveTitleTextSize) * selectedPercentage
        color = ColorUtils.colorInterColors(inactiveColor, activeColor, selectedPercentage)

        val cx = bounds.centerX()
        val cy = bounds.centerY()

        val titleOffsetY = inactiveTitleOffsetY + (activeTitleOffsetY - inactiveTitleOffsetY) * selectedPercentage
        titleDrawPoint.set(cx, cy + titleOffsetY)

        val iconOffsetY = inactiveIconOffsetY + (activeIconOffsetY - inactiveIconOffsetY) * selectedPercentage
        icon.setBounds(
                (cx - iconSize.x / 2).toInt(),
                (cy + iconOffsetY - iconSize.y).toInt(),
                (cx + iconSize.x / 2).toInt(),
                (cy + iconOffsetY).toInt()
        )
    }

    fun setBounds(left: Float, top: Float, right: Float, bottom: Float) {
        bounds.set(left, top, right, bottom)
        updateDrawInfo()
    }

    fun contains(x: Float, y: Float) =
            bounds.contains(x, y)

    fun handleMotionEvent(event: MotionEvent) {
        rippleDrawer.handleMotionEvent(event)

        if (event.action == MotionEvent.ACTION_UP && contains(event.x, event.y)) {
            onClicked.invoke()
        }
    }

    fun draw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        icon.draw(canvas)
        canvas.drawText(title, titleDrawPoint.x, titleDrawPoint.y, titlePaint)
    }

    fun onSelectedTabChanged(position: Int) {
        selectedAnimator.animateTo(position == pos)
    }

    private fun onNeedRefresh(param: Unit) = onNeedRefresh.invoke()

    private fun onSelectedPercentageChanged(selectedPercentage: Float) {
        this.selectedPercentage = selectedPercentage
    }

    fun onAttachedToWindow() {
        rippleDrawer.attach(this::onNeedRefresh)
        selectedAnimator.attach(this::onSelectedPercentageChanged)
    }

    fun onDetachedFromWindow() {
        rippleDrawer.detach(this::onNeedRefresh)
        selectedAnimator.detach(this::onSelectedPercentageChanged)
    }

}