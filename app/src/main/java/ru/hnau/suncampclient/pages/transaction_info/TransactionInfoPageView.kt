package ru.hnau.suncampclient.pages.transaction_info

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.header.PageViewWithHeader
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class TransactionInfoPageView(
        context: Context,
        onBackClicked: () -> Unit,
        transaction: Transaction
) : PageViewWithHeader<TransactionInfoPageInnerView>(
        context = context,
        title = StringGetter(R.string.transaction_info_page_title),
        rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
        onBackClicked = onBackClicked,
        contentView = TransactionInfoPageInnerView(context, transaction),
        headerInfo = ColorManager.HEADER_INFO,
        headerBackButtonInfo = ColorManager.HEADER_BACK_BUTTON_INFO,
        headerLabelInfo = ColorManager.HEADER_LABEL_INFO
)