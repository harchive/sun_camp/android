package ru.hnau.suncampclient.pages.main.view.tabs

import android.graphics.Canvas
import android.support.v4.view.ViewPager
import android.view.MotionEvent
import android.view.View
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.jutils.takeIfPositive


class TabsButtons(
        tabs: List<Tab>,
        private val pager: ViewPager
) : View(pager.context) {

    companion object {

        private val PREFERRED_HEIGHT = DpPxGetter.fromDp(56)

    }

    private val tabsCount = tabs.size

    private val buttons = tabs.mapIndexed { i, tab ->
        TabButton(
                context = context,
                onClicked = { onTabClicked(i) },
                onNeedRefresh = this::invalidate,
                pos = i,
                tab = tab
        )
    }

    private var pressedButton: TabButton? = null

    init {
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) = onTabChanged(position)

        })

        onTabChanged(0)
    }

    private fun onTabChanged(tabPos: Int) {
        buttons.forEach { it.onSelectedTabChanged(tabPos) }
    }


    private fun onTabClicked(pos: Int) {
        pager.currentItem = pos
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        val pressedButton = this.pressedButton
        if (pressedButton != null) {
            pressedButton.handleMotionEvent(event)
            if (event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL) {
                this.pressedButton = null
            }
            return true
        }

        this.pressedButton = if (event.action != MotionEvent.ACTION_DOWN) null else
            buttons.find { it.contains(event.x, event.y) }
        this.pressedButton?.handleMotionEvent(event)

        return true
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        buttons.forEach { it.draw(canvas) }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        tabsCount.takeIfPositive() ?: return

        val buttonWidth = width.toFloat() / tabsCount.toFloat()
        val buttonHeight = height.toFloat()

        buttons.forEachIndexed { i, tabButton ->
            tabButton.setBounds(
                    left = i * buttonWidth,
                    bottom = buttonHeight,
                    right = (i + 1) * buttonWidth,
                    top = 0f
            )
        }

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getMaxMeasurement(widthMeasureSpec, 0)
        val height = getDefaultMeasurement(heightMeasureSpec, PREFERRED_HEIGHT.getPxInt(context))
        setMeasuredDimension(width, height)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        buttons.forEach(TabButton::onAttachedToWindow)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        buttons.forEach(TabButton::onDetachedFromWindow)
    }

}