package ru.hnau.suncampclient.pages.main.view.storages

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.androidutils.ui.view.list.group.GroupList
import ru.hnau.androidutils.ui.view.list.group.ListGroup
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.suncamp.common.api.response.SubordinateStorageInfo
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.GroupListTitleWrapper


class SubordinateStoragesListView(
        context: Context,
        storages: List<Pair<SubordinateStorageInfo, User>>,
        onStorageClicked: (Pair<SubordinateStorageInfo, User>) -> Unit
) : GroupList<StringGetter, Pair<SubordinateStorageInfo, User>>(
        context = context,
        orientation = BaseListOrientation.VERTICAL,
        itemsProducer = DataProducer(storages).map {
            listOf(
                    ListGroup(
                            group = StringGetter(R.string.main_page_storages_title),
                            items = it
                    )
            )
        },
        groupsViewWrappersCreator = { GroupListTitleWrapper(context) },
        itemsViewWrappersCreator = { SubordinateStoragesListItemView(context, onStorageClicked) },
        fixedSize = true
)