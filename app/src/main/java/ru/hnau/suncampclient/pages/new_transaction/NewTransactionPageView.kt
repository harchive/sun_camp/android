package ru.hnau.suncampclient.pages.new_transaction

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.header.PageViewWithHeader
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.pages.new_transaction.param.NewTransactionParam
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class NewTransactionPageView(
        context: Context,
        param: NewTransactionParam,
        onBackClicked: () -> Unit
) : PageViewWithHeader<NewTransactionPageInnerView>(
        context = context,
        title = StringGetter(R.string.new_transaction_page_title),
        rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
        onBackClicked = onBackClicked,
        contentView = NewTransactionPageInnerView(context, param),
        headerInfo = ColorManager.HEADER_INFO,
        headerBackButtonInfo = ColorManager.HEADER_BACK_BUTTON_INFO,
        headerLabelInfo = ColorManager.HEADER_LABEL_INFO
)