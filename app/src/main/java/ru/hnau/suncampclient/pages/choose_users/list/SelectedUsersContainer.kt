package ru.hnau.suncampclient.pages.choose_users.list

import ru.hnau.jutils.producer.DataProducer
import ru.hnau.suncamp.common.data.User


class SelectedUsersContainer : DataProducer<Set<User>>(emptySet()) {

    val selectedUsers: Set<User>
        get() = data

    fun onUserClicked(user: User) = synchronized(this) {
        val oldData = data
        val newData = if (user in oldData) {
            oldData - user
        } else {
            oldData + user
        }
        data = newData.toSet()
    }

    fun selectAll(allUsers: List<User>) {
        data = allUsers.toSet()
    }

    fun unselectAll() {
        data = emptySet()
    }

}