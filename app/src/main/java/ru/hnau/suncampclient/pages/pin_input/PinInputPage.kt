package ru.hnau.suncampclient.pages.pin_input

import android.content.Context
import android.view.View
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.pager.Page


open class PinInputPage(
        private val signsCount: Int,
        private val title: StringGetter,
        private val afterInput: (String, PinInputPageConnector) -> Unit,
        private val additionalViewBuilder: ((Context, PinInputPageConnector) -> View)? = null
) : Page<PinInputPageView>() {

    override fun generateView(context: Context) =
            PinInputPageView(
                    context,
                    signsCount,
                    title,
                    afterInput,
                    additionalViewBuilder,
                    this::showPage,
                    this::goBack
            )

}