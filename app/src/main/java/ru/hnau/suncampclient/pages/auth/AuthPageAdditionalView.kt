package ru.hnau.suncampclient.pages.auth

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setFrameLayoutLayoutParams
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.pages.login.LoginPage
import ru.hnau.suncampclient.pages.pin_input.PinInputPageConnector
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class AuthPageAdditionalView(
        context: Context,
        private val pinInputPageConnector: PinInputPageConnector
) : FrameLayout(
        context
) {

    private val changeUserButton = SunButton(
            context = context,
            text = StringGetter(R.string.auth_page_change_user),
            onClick = this::onChangeUserButtonClicked,
            color = SunButtonColorInfo.WHITE_ON_RED,
            size = SunButtonSizeInfo.SIZE_16,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO
    ).apply {
        setFrameLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            gravity = Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL
        }
    }

    init {
        addView(changeUserButton)
    }

    private fun onChangeUserButtonClicked() {
        pinInputPageConnector.showPage(LoginPage(), true)
    }

}