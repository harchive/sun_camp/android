package ru.hnau.suncampclient.pages.qr_code_scan.camera

import com.google.zxing.*
import com.google.zxing.common.HybridBinarizer
import ru.hnau.androidutils.utils.runUi
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.tryCatch
import ru.hnau.jutils.tryOrNull
import kotlin.concurrent.thread


class BarcodeDecoder(
        private val snapshotsProducer: CameraSnapshotsProducer
) : Producer<String>() {

    private val decoder = MultiFormatReader().apply {
        setHints(mapOf(DecodeHintType.POSSIBLE_FORMATS to listOf(BarcodeFormat.QR_CODE)))
    }


    private var decodingThread: Thread? = null

    private var decodingSnapshot: CameraSnapshot? = null

    private val snapshotLock = Object()

    private fun onSnapshotReceived(snapshot: CameraSnapshot) {
        synchronized(this) {
            if (decodingSnapshot != null) {
                return
            }
            synchronized(snapshotLock) {
                decodingSnapshot = snapshot
                snapshotLock.notifyAll()
            }
        }
    }

    private fun decodeSnapshot(snapshot: CameraSnapshot) {
        val source = PlanarYUVLuminanceSource(snapshot.bytes, snapshot.width, snapshot.height, 0, 0, snapshot.width, snapshot.height, false)
        val bitmap = BinaryBitmap(HybridBinarizer(source))
        val res = tryOrNull { decoder.decode(bitmap).text!! } ?: return
        runUi { call(res) }
    }

    override fun onFirstAttached() {
        super.onFirstAttached()
        snapshotsProducer.attach(this::onSnapshotReceived)

        decodingThread = thread {
            while (Thread.currentThread() == decodingThread) {
                synchronized(snapshotLock) {
                    if (decodingSnapshot == null) {
                        tryCatch { snapshotLock.wait() }
                    }
                    decodingSnapshot?.let { snapshot ->
                        decodeSnapshot(snapshot)
                        decodingSnapshot = null
                    }
                }
            }
        }

    }

    override fun onLastDetached() {
        super.onLastDetached()
        snapshotsProducer.detach(this::onSnapshotReceived)
        decodingThread = null
    }

}