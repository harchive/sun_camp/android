package ru.hnau.suncampclient.pages.new_version

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.header.Header
import ru.hnau.androidutils.ui.view.header.HeaderTitle
import ru.hnau.androidutils.ui.view.header.back.button.HeaderBackButton
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.jutils.tryCatch
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.sun_button.SunButton
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager


@SuppressLint("ViewConstructor")
class NewVersionPageView(
        context: Context,
        info: NewVersionInfo
) : LinearLayout(
        context
) {

    private val header = Header(
            context = context,
            headerInfo = ColorManager.HEADER_INFO
    ).apply {

        if (!info.hard) {
            addView(HeaderBackButton(
                    context = context,
                    color = ColorManager.BROWN,
                    rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
                    onClick = { AppActivityConnector.goBack() }
            ))
        }

        addView(HeaderTitle(
                context = context,
                initialText = StringGetter(R.string.new_version_page_title),
                info = ColorManager.HEADER_LABEL_INFO
        ))


    }

    private val descriptionView = Label(
            context = context,
            initialText = info.description.toGetter(),
            gravity = HGravity.CENTER,
            textSize = DpPxGetter.fromDp(20),
            textColor = ColorManager.YELLOW,
            fontType = FontManager.PT_SANS
    ).apply {
        setPadding(SizeManager.DEFAULT_PADDING)
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f)
    }

    private val updateButton = info.updateUrl?.let { updateUrl ->
        SunButton(
                context = context,
                color = SunButtonColorInfo.BROWN_ON_YELLOW,
                onClick = { updateButtonClicked(updateUrl) },
                text = StringGetter(R.string.new_version_page_button_update),
                shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
                size = SunButtonSizeInfo.SIZE_24
        )
    }

    init {
        orientation = VERTICAL
        addView(header)
        addView(descriptionView)
        updateButton?.let(this::addView)
    }

    private fun updateButtonClicked(url: String) = tryCatch {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        context.startActivity(browserIntent)
    }


}