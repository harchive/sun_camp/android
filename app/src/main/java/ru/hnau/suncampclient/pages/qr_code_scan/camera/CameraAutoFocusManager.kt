package ru.hnau.suncampclient.pages.qr_code_scan.camera

import android.hardware.Camera
import android.os.Handler
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.tryCatch


class CameraAutoFocusManager(
        private val camera: Camera
) {

    companion object {

        private val AUTO_FOCUS_PERIOD = TimeValue.SECOND * 3

    }

    private var stopped = false

    private val handler = Handler()

    private var autoFocusAction = {}

    init {

        autoFocusAction = {
            tryCatch({
                camera.autoFocus { _, _ ->
                    addHandlerCallbackIfNotStopped()
                }
            }, {
                addHandlerCallbackIfNotStopped()
            })
        }

        addHandlerCallbackIfNotStopped()

    }

    private fun addHandlerCallbackIfNotStopped() {
        if (!stopped) {
            handler.postDelayed(autoFocusAction, AUTO_FOCUS_PERIOD.milliseconds)
        }
    }

    fun release() {
        stopped = true
        handler.removeCallbacks(autoFocusAction)
    }


}