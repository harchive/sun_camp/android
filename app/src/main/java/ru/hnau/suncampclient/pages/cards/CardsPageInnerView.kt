package ru.hnau.suncampclient.pages.cards

import android.content.Context
import android.view.Gravity
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawableLayoutType
import ru.hnau.androidutils.ui.view.AutoSwipeRefreshView
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButtonSize
import ru.hnau.androidutils.ui.view.buttons.circle.icon.CircleIconButton
import ru.hnau.androidutils.ui.view.buttons.circle.icon.CircleIconButtonInfo
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setFrameLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.MyCardsManager
import ru.hnau.suncampclient.utils.ColorManager


class CardsPageInnerView(context: Context) : AutoSwipeRefreshView(
        context = context,
        color = ColorManager.RED
) {

    private val finishersLockedProducer = FinishersLockedProducer()

    private val waiterView = ColorManager.createWaiter(context, finishersLockedProducer)

    private val contentView = CardsPageContentView(context, finishersLockedProducer)

    private val bindButton = CircleIconButton(
            context = context,
            size = CircleButtonSize.DP_56,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
            rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
            onClick = CardsPage.Companion::bindCard,
            info = CircleIconButtonInfo.DEFAULT,
            icon = LayoutDrawable(
                    context = context,
                    content = DrawableGetter(R.drawable.ic_add_brown),
                    layoutType = LayoutDrawableLayoutType.WRAP_CONTENT
            ).toGetter()
    ).apply {
        setPadding(DpPxGetter.fromDp(6))
        setFrameLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            gravity = Gravity.RIGHT or Gravity.BOTTOM
        }
    }

    private val contentContainer = FrameLayout(context).apply {
        addView(contentView)
        addView(bindButton)
        addView(waiterView)
    }

    init {
        addView(contentContainer)
    }

    override fun updateContent() = MyCardsManager.invalidate()

}