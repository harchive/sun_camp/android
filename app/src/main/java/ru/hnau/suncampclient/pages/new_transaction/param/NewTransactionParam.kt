package ru.hnau.suncampclient.pages.new_transaction.param

import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncamp.common.data.TransactionCategory
import ru.hnau.suncampclient.pages.new_transaction.TransactionOrNull


abstract class NewTransactionParam(
        val amount: Long? = null,
        val category: TransactionCategory = TransactionCategory.payment,
        val comment: String = ""
) {

    abstract fun execute(
            amount: Long,
            category: TransactionCategory,
            comment: String
    ): Finisher<Possible<TransactionOrNull>>

}