package ru.hnau.suncampclient.pages.cards

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.view.MotionEvent
import android.view.View
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawableLayoutType
import ru.hnau.androidutils.ui.ripple.RippleDrawInfo
import ru.hnau.androidutils.ui.ripple.RippleDrawer
import ru.hnau.androidutils.ui.ripple.RippleDrawerPathShapeInfo
import ru.hnau.androidutils.ui.utils.drawing.doInState
import ru.hnau.androidutils.ui.utils.drawing.initAsRoundCornerRect
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.utils.shadow.drawer.ButtonShadowDrawer
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawerPathShape
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.finisher.awaitSuccess
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.suncamp.common.data.Card
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.api.Api
import ru.hnau.suncampclient.data.MyCardsManager
import ru.hnau.suncampclient.ui.view.qr_code.QrCodeDrawable
import ru.hnau.suncampclient.ui.view.qr_code.QrCodeView
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager
import ru.hnau.suncampclient.utils.dialog_manager.list.DialogManagerListViewItem


@SuppressLint("ViewConstructor")
class CardsPageListItemView(
        context: Context,
        private val finishersLockedProducer: FinishersLockedProducer
) : View(
        context
), BaseListViewWrapper<Card> {

    companion object {

        private const val CARD_ASPECT_RATIO = 54f / 86f
        private const val CORNER_RADIUS_BY_CARD_WIDTH = 3f / 86f
        private const val QR_CODE_SIZE_BY_CARD_WIDTH = 40f / 86f
        private const val QR_CODE_LEFT_BY_CARD_WIDTH = 10f / 86f

    }

    private val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = ColorManager.BROWN.get(context)
    }

    private val backgroundDrawable = LayoutDrawable(
            context = context,
            gravity = HGravity.LEFT,
            layoutType = LayoutDrawableLayoutType.PROPORTIONAL_IN,
            content = DrawableGetter(R.drawable.ic_card_background)
    )

    private val qrCodeDrawable = QrCodeDrawable(context)

    private val memSaveRect = RectF()

    private val cardRect = RectF()
    private val cardPath = Path()

    private val shadowDrawer = ButtonShadowDrawer(
            context = context,
            shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
            shape = ShadowDrawerPathShape(cardPath)
    ).apply {
        attach { invalidate() }
    }

    private val rippleDrawer = RippleDrawer(
            context = context,
            rippleDrawInfo = RippleDrawInfo(
                    rippleInfo = ColorManager.DEFAULT_RIPPLE_INFO,
                    color = ColorManager.BROWN,
                    backgroundColor = ColorManager.TRANSPARENT
            ),
            shapeInfo = RippleDrawerPathShapeInfo(cardPath)
    ).apply {
        attach { invalidate() }
    }

    private var card: Card? = null

    override val view: View
        get() = this

    init {
        setSoftwareRendering()
        val paddingHorizontal = DpPxGetter.fromDp(16)
        val paddingTop = DpPxGetter.fromDp(16)
        setPadding(paddingHorizontal, paddingTop, paddingHorizontal, DpPxGetter.ZERO)
    }

    override fun setContent(content: Card) {
        this.card = content
        qrCodeDrawable.code = content.uuid
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        shadowDrawer.draw(canvas)
        canvas.drawPath(cardPath, backgroundPaint)
        canvas.doInState {
            clipPath(cardPath)
            backgroundDrawable.draw(canvas)
        }
        qrCodeDrawable.draw(canvas)
        rippleDrawer.draw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (event.action == MotionEvent.ACTION_DOWN) {
            if (isEventInButton(event)) {
                shadowDrawer.animateToPressed()
            } else {
                return false
            }
        }

        if (event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL) {
            shadowDrawer.animateToNormal()
        }

        if (event.action == MotionEvent.ACTION_UP && isEventInButton(event)) {
            onClicked()
        }

        rippleDrawer.handleMotionEvent(event)

        return true
    }

    private fun onClicked() {
        val card = this.card ?: return
        DialogManager.show<Unit> {
            list(
                    listOf(
                            DialogManagerListViewItem(
                                    icon = DrawableGetter(R.drawable.ic_qr),
                                    title = StringGetter(R.string.card_page_action_title_qr),
                                    subtitle = StringGetter(R.string.card_page_action_subtitle_qr),
                                    action = {
                                        DialogManager.show<Unit> { view(QrCodeView(context, card.uuid)) }
                                    }
                            ),
                            DialogManagerListViewItem(
                                    icon = DrawableGetter(R.drawable.ic_remove),
                                    title = StringGetter(R.string.card_page_action_title_remove),
                                    subtitle = StringGetter(R.string.card_page_action_subtitle_remove),
                                    action = {
                                        Api.cardRemove(card.uuid)
                                                .addToLocker(finishersLockedProducer)
                                                .awaitSuccess {
                                                    MyCardsManager.invalidate()
                                                }
                                    }
                            )
                    )
            )
        }
    }

    private fun isEventInButton(event: MotionEvent) =
            cardRect.contains(event.x, event.y)

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        cardRect.set(
                paddingLeft + shadowDrawer.shadowSizeRect.left,
                paddingTop + shadowDrawer.shadowSizeRect.top,
                width - paddingRight - shadowDrawer.shadowSizeRect.right,
                height - paddingBottom - shadowDrawer.shadowSizeRect.bottom
        )
        val cardWidth = cardRect.width()
        val cornerRadius = cardWidth * CORNER_RADIUS_BY_CARD_WIDTH
        cardPath.initAsRoundCornerRect(cardRect, cornerRadius, memSaveRect)

        val qrCodeSize = (cardWidth * QR_CODE_SIZE_BY_CARD_WIDTH).toInt()
        val qrCodeLeft = (cardRect.left + cardWidth * QR_CODE_LEFT_BY_CARD_WIDTH).toInt()
        val qrCodeTop = (cardRect.top + (cardRect.height() - qrCodeSize) / 2).toInt()
        qrCodeDrawable.setBounds(
                qrCodeLeft,
                qrCodeTop,
                qrCodeLeft + qrCodeSize,
                qrCodeTop + qrCodeSize
        )

        backgroundDrawable.setBounds(
                cardRect.left.toInt(),
                cardRect.top.toInt(),
                cardRect.right.toInt(),
                cardRect.bottom.toInt()
        )


    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getMaxMeasurement(widthMeasureSpec, 0)
        val cardWidth = width - horizontalPaddingSum - shadowDrawer.shadowSizeRect.right - shadowDrawer.shadowSizeRect.left
        val cardHeight = cardWidth * CARD_ASPECT_RATIO
        val preferredHeight = (cardHeight + verticalPaddingSum + shadowDrawer.shadowSizeRect.top + shadowDrawer.shadowSizeRect.bottom).toInt()
        val height = getDefaultMeasurement(heightMeasureSpec, preferredHeight)
        setMeasuredDimension(width, height)
    }

}