package ru.hnau.suncampclient.pages.choose_users

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.header.PageViewWithHeader
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class ChooseUsersPageView(
        context: Context,
        title: StringGetter,
        onUsersWasChosen: (Set<User>, ChooseUsersPageConnector) -> Unit,
        showPage: (page: Page<*>, clearStack: Boolean, precedingPages: List<Page<*>>) -> Boolean,
        goBack: (pageResolver: (List<Page<*>>) -> Int?) -> Boolean,
        onBackClicked: () -> Unit
) : PageViewWithHeader<ChooseUsersPageInnerView>(
        context = context,
        title = title,
        rippleDrawInfo = ColorManager.YELLOW_RIPPLE_DRAW_INFO,
        onBackClicked = onBackClicked,
        contentView = ChooseUsersPageInnerView(context, onUsersWasChosen, showPage, goBack),
        headerInfo = ColorManager.HEADER_INFO,
        headerBackButtonInfo = ColorManager.HEADER_BACK_BUTTON_INFO,
        headerLabelInfo = ColorManager.HEADER_LABEL_INFO
)