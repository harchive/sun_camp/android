package ru.hnau.suncampclient.pages.main.view.tabs

import android.content.Context
import android.view.View
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter


data class Tab(
        val viewGenerator: (Context) -> View,
        val title: StringGetter,
        val icon: DrawableGetter
)