package ru.hnau.suncampclient.pages.qr_code_scan

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.hardware.Camera
import android.view.View
import android.widget.FrameLayout
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.jutils.producer.locked_producer.LockedProducersContainer
import ru.hnau.jutils.producer.locked_producer.SimpleLockedProducer
import ru.hnau.suncampclient.pages.qr_code_scan.camera.BarcodeDecoder
import ru.hnau.suncampclient.pages.qr_code_scan.camera.CameraManager
import ru.hnau.suncampclient.pages.qr_code_scan.camera.CameraPreviewView
import ru.hnau.suncampclient.pages.qr_code_scan.camera.CameraSnapshotsProducer
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class QrCodeScanPageInnerView(
        context: Context,
        private val afterScan: (String, QrCodeScanPageConnector) -> Unit,
        decorationViewBuilder: ((Context, QrCodeScanPageConnector) -> View)? = null,
        private val showPage: (page: Page<*>, clearStack: Boolean, precedingPages: List<Page<*>>) -> Boolean,
        private val goBack: (pageResolver: (List<Page<*>>) -> Int?) -> Boolean
) : FrameLayout(
        context
), QrCodeScanPageConnector {

    override val finishersLockedProducer = FinishersLockedProducer()

    private val simpleLockedProducer = SimpleLockedProducer()

    private val lockedProducer = LockedProducersContainer(finishersLockedProducer, simpleLockedProducer)

    private val waiterView = ColorManager.createWaiter(context, lockedProducer)

    private val snapshotsProducer = CameraSnapshotsProducer()

    private val decodingResultProducer = BarcodeDecoder(
            snapshotsProducer = snapshotsProducer
    )

    override var scanning: Boolean
        set(value) {
            snapshotsProducer.producing = value
        }
        get() = snapshotsProducer.producing

    private var attachedToWindow = false
        set(value) {
            if (field != value) {
                synchronized(this) {
                    field = value
                    visibilityState = value && visible
                }
            }
        }

    private var visible = true
        set(value) {
            if (field != value) {
                synchronized(this) {
                    field = value
                    visibilityState = value && attachedToWindow
                }
            }
        }

    private var visibilityState = false
        set(value) {
            if (field != value) {
                field = value
                onVisibilityStateChanged(value)
            }
        }

    private val previewContainer = FrameLayout(context)

    private var previewView: CameraPreviewView? = null
        set(value) {
            field?.snapshotsProducer?.detach(snapshotsProducer::onNewSnapshot)
            value?.snapshotsProducer?.attach(snapshotsProducer::onNewSnapshot)
            field = value
            if (value != null) {
                previewContainer.addView(previewView)
            } else {
                previewContainer.removeAllViews()
            }
        }

    private val decorationView = decorationViewBuilder?.invoke(context, this)
            ?: QrCodeScanPageDecorationView(context)

    init {
        setBackgroundColor(Color.BLACK)
        setOnClickListener { }

        addView(previewContainer)
        decorationView.let { addView(it) }
        addView(waiterView)

    }

    private fun onCameraChanged(camera: Possible<Camera>) {
        simpleLockedProducer.setIsLocked(camera.data == null)
        previewView = camera.data?.let { CameraPreviewView(context, it) }
    }

    private fun onVisibilityStateChanged(visible: Boolean) {
        if (visible) {
            CameraManager.attach(this::onCameraChanged)
        } else {
            CameraManager.detach(this::onCameraChanged)
        }
    }

    override fun onVisibilityChanged(changedView: View?, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        visible = visibility == VISIBLE
    }

    private fun onDecoded(scanResult: String) {
        afterScan.invoke(scanResult, this)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        attachedToWindow = true
        decodingResultProducer.attach(this::onDecoded)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        attachedToWindow = false
        decodingResultProducer.detach(this::onDecoded)
    }

    override fun showPage(page: Page<*>, clearStack: Boolean, precedingPages: List<Page<*>>) =
            showPage.invoke(page, clearStack, precedingPages)

    override fun goBack(pageResolver: (List<Page<*>>) -> Int?) =
            goBack.invoke(pageResolver)

}