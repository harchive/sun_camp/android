package ru.hnau.suncampclient.pages.choose_users.list

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.finisher.await
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.Producer
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.UsersBalanceManager
import ru.hnau.suncampclient.ui.view.SunClickableFrameLayout
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager


@SuppressLint("ViewConstructor")
class ChooseUsersPageUsersListItemView(
        context: Context,
        private val selectedUsersProducer: Producer<Set<User>>,
        private val onUserClicked: (User) -> Unit
) : SunClickableFrameLayout(
        context = context,
        rippleDrawInfo = ColorManager.WHITE_RIPPLE_DRAW_INFO,
        shadowInfo = ColorManager.BUTTON_SHADOW_INFO
), BaseListViewWrapper<User> {

    private var selectedUsers = emptySet<User>()
        private set(value) {
            field = value
            updateIsSelected()
        }

    private var user: User? = null
        set(value) {
            field = value
            updateIsSelected()
        }

    private var isSelectedUser = false
        set(value) {
            if (field != value) {
                field = value
                onSelectedStateChanged(value)
            }
        }

    override val view: View
        get() = this

    private val iconView = ImageView(context).apply {
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
    }

    private val nameView = Label(
            context = context,
            maxLines = 2,
            minLines = 2,
            textSize = DpPxGetter.fromDp(20),
            gravity = HGravity.LEFT_CENTER_VERTICAL,
            fontType = FontManager.PT_SANS,
            textColor = ColorManager.BROWN
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f) {
            setMargins(SizeManager.DEFAULT_PADDING.getPxInt(context), 0)
        }
    }

    private val balanceView = Label(
            context = context,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(20),
            gravity = HGravity.RIGHT_CENTER_VERTICAL,
            fontType = FontManager.PT_SANS,
            textColor = ColorManager.ORANGE
    ).apply {
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            setRightMargin(SizeManager.DEFAULT_PADDING.getPxInt(context))
        }
    }

    private val userSelectedSignView = ImageView(context).apply {
        val size = DpPxGetter.fromDp(24)
        setLinearLayoutLayoutParams(size, size)
    }

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        setPadding(SizeManager.DEFAULT_PADDING)
        gravity = Gravity.CENTER
        addView(iconView)
        addView(nameView)
        addView(balanceView)
        addView(userSelectedSignView)
    }

    private var usersBalances = emptyMap<String, Long>()
        set(value) {
            field = value
            onBalanceChanged()
        }

    init {
        addView(contentContainer)
    }

    private fun onBalanceChanged() {
        val balance = user?.login?.let(usersBalances::get)
        balanceView.text = (balance ?: 0L).toString().toGetter()
        balanceView.visibility = if (balance == null) View.GONE else View.VISIBLE
    }

    override fun onClicked() {
        super.onClicked()
        val user = this.user ?: return
        onUserClicked.invoke(user)
    }

    private fun updateIsSelected() {
        isSelectedUser = user?.takeIf { it in selectedUsers } != null
    }

    private fun onSelectedStateChanged(selected: Boolean) {
        if (selected) {
            userSelectedSignView.setImageResource(R.drawable.ic_selected)
            return
        }
        userSelectedSignView.setImageDrawable(null)
    }

    override fun setContent(content: User) {
        this.user = content
        onBalanceChanged()
        nameView.text = StringGetter(content.name)
        iconView.setImageResource(if (content.type == UserType.storage) R.drawable.ic_storage else R.drawable.ic_user)
    }

    private fun onSelectedUsersChanged(selectedUsers: Set<User>) {
        this.selectedUsers = selectedUsers
    }

    private fun onUsersBalancesFinisherReceived(finisher: Finisher<Possible<Map<String, Long>>>) {
        finisher.await(
                onSuccess = { usersBalances = it },
                onError = { usersBalances = emptyMap() }
        )
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        selectedUsersProducer.attach(this::onSelectedUsersChanged)
        UsersBalanceManager.attach(this::onUsersBalancesFinisherReceived)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        selectedUsersProducer.detach(this::onSelectedUsersChanged)
        UsersBalanceManager.detach(this::onUsersBalancesFinisherReceived)
    }


}