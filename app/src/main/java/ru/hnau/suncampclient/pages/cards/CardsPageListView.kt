package ru.hnau.suncampclient.pages.cards

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.ui.view.list.base.BaseList
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.suncamp.common.data.Card


@SuppressLint("ViewConstructor")
class CardsPageListView(
        context: Context,
        cards: List<Card>,
        finishersLockedProducer: FinishersLockedProducer
) : BaseList<Card>(
        context = context,
        fixedSize = true,
        orientation = BaseListOrientation.VERTICAL,
        itemsProducer = DataProducer(cards),
        viewWrappersCreator = { CardsPageListItemView(context, finishersLockedProducer) }
)