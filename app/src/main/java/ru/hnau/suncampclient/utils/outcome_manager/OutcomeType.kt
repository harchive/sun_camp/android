package ru.hnau.suncampclient.utils.outcome_manager

import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.pages.choose_outcome_user.ChooseOutcomeUserPage
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager
import ru.hnau.suncampclient.utils.dialog_manager.list.DialogManagerListViewItem


enum class OutcomeType(
        val title: StringGetter,
        val action: () -> Unit,
        val icon: DrawableGetter,
        val subtitle: StringGetter
) {
    USER(
            icon = DrawableGetter(R.drawable.ic_user),
            title = StringGetter(R.string.outcome_type_title_user),
            subtitle = StringGetter(R.string.outcome_type_subtitle_user),
            action = {
                AppActivityConnector.showPage(ChooseOutcomeUserPage())
            }
    ),

    CARD(
            icon = DrawableGetter(R.drawable.ic_card),
            title = StringGetter(R.string.outcome_type_title_card),
            subtitle = StringGetter(R.string.outcome_type_subtitle_card),
            action = OutcomeManager::outcomeToCard
    );

    val dialogManagerListItem = DialogManagerListViewItem(
            title = title,
            subtitle = subtitle,
            icon = icon,
            action = action
    )

}