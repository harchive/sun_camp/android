package ru.hnau.suncampclient.utils.dialog_manager

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.view.MotionEvent
import android.view.View
import ru.hnau.androidutils.animations.TwoStatesAnimator
import ru.hnau.androidutils.go_back_handler.GoBackHandler
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.androidutils.ui.view.view_changer.ViewChanger
import ru.hnau.jutils.TimeValue


class DialogManagerView(context: Context) : ViewChanger(
        context = context,
        animationTime = ANIMATION_TIME,
        scrollFactor = 0.05f,
        gravity = HGravity.CENTER,
        fromSide = Side.BOTTOM,
        hideInterpolator = FastOutLinearInInterpolator(),
        showInterpolator = LinearOutSlowInInterpolator()
), GoBackHandler {

    companion object {
        private const val MAX_SHADOW_ALPHA = 159
        private val ANIMATION_TIME = TimeValue.MILLISECOND * 200
    }

    private val placeholderView = object : View(context) {

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            setMeasuredDimension(
                    getMaxMeasurement(widthMeasureSpec, 0),
                    getMaxMeasurement(heightMeasureSpec, 0)
            )
        }

    }

    private val shadowPaint = Paint().apply {
        color = Color.BLACK
        alpha = 0
    }

    private val shadowAnimator = TwoStatesAnimator(
            switchingTime = ANIMATION_TIME
    )

    private var viewExists: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                shadowAnimator.animateTo(value)
            }
        }

    private var currentViewClosedCallback: (() -> Unit)? = null

    init {
        hideCurrentView()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (viewExists) {
            if (event.action == MotionEvent.ACTION_UP) {
                hideCurrentView()
            }
            return true
        }

        return super.onTouchEvent(event)
    }

    fun hideCurrentView() = showNewView(null)

    fun showNewView(view: View?, afterClosed: (() -> Unit)? = null) {
        viewExists = view != null
        currentViewClosedCallback?.invoke()
        currentViewClosedCallback = afterClosed
        val viewToShow = view?.let(::DialogDecorationView) ?: placeholderView
        showView(viewToShow)
    }

    override fun dispatchDraw(canvas: Canvas) {
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), shadowPaint)
        super.dispatchDraw(canvas)
    }

    private fun onShadowPercentageChanged(shadowPercentage: Float) {
        shadowPaint.alpha = (MAX_SHADOW_ALPHA * shadowPercentage).toInt()
        invalidate()
    }

    override fun handleGoBack(): Boolean {
        if (!viewExists) {
            return false
        }
        hideCurrentView()
        return true
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        shadowAnimator.attach(this::onShadowPercentageChanged)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        shadowAnimator.detach(this::onShadowPercentageChanged)
    }

}