package ru.hnau.suncampclient.utils.timestamp

import java.text.SimpleDateFormat
import java.util.*


enum class TimestampShowingType(
        format: String
) {

    TIME("HH:mm"),
    MONTH_AND_DAY("dd/MM"),
    DATE("dd/MM/yyyy");


    private val dateFormat = SimpleDateFormat(format, Locale("ru"))

    fun formatTimestamp(timestamp: Long) = dateFormat.format(Date(timestamp))!!

}