package ru.hnau.suncampclient.utils

import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.jutils.possible.Possible
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager


object PermissionExplanationShower {

    fun getOnNeedShowExplanation(
            text: StringGetter
    ) = {
        DialogManager.show<Unit> {
            title(text)
            buttons {
                button(
                        text = StringGetter(R.string.permission_requester_explanation_dialog_button_ok),
                        onClick = { Possible.success(Unit) }
                )
            }
        }.map { _ ->
            Unit
        }
    }

}