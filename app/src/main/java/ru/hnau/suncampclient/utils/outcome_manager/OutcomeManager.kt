package ru.hnau.suncampclient.utils.outcome_manager

import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.utils.showToast
import ru.hnau.jutils.finisher.await
import ru.hnau.jutils.finisher.awaitSuccess
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.tryCatch
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.api.Api
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.pages.new_transaction.NewTransactionPage
import ru.hnau.suncampclient.pages.new_transaction.param.NewTransactionParam
import ru.hnau.suncampclient.pages.new_transaction.param.NewUserToUserTransactionParam
import ru.hnau.suncampclient.pages.qr_code_scan.QrCodeScanPage
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonSizeInfo
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.LoginManager
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager
import ru.hnau.suncampclient.utils.dialog_manager.list.DialogManagerListViewItem
import java.util.*


object OutcomeManager {

    fun doOutcome() {
        DialogManager.show<Unit> {
            list(OutcomeType.values().map { it.dialogManagerListItem })
        }
    }

    fun outcomeToCard() {

        AppActivityConnector.showPage(
                QrCodeScanPage(
                        afterScan = { cardUuid, qrCodeScanPageConnector ->

                            val isUUID = tryCatch { UUID.fromString(cardUuid) }
                            if (!isUUID) {
                                return@QrCodeScanPage
                            }

                            qrCodeScanPageConnector.scanning = false
                            Api.cardGetUserByCardUuid(cardUuid)
                                    .addToLocker(qrCodeScanPageConnector.finishersLockedProducer)
                                    .await(
                                            onSuccess = { user ->

                                                qrCodeScanPageConnector.scanning = true

                                                if (user.login == LoginManager.login) {
                                                    showToast(StringGetter(R.string.outcome_circle_transaction_not_allowed))
                                                    return@await
                                                }

                                                UsersManager.getByLogin(LoginManager.login)
                                                        .addToLocker(qrCodeScanPageConnector.finishersLockedProducer)
                                                        .awaitSuccess { me ->
                                                            qrCodeScanPageConnector.showPage(
                                                                    NewTransactionPage(
                                                                            NewUserToUserTransactionParam(
                                                                                    fromUser = me,
                                                                                    toUser = user
                                                                            )
                                                                    )
                                                            )
                                                        }


                                            },
                                            onError = {
                                                qrCodeScanPageConnector.scanning = true
                                            }
                                    )

                        }
                )
        )
    }

}