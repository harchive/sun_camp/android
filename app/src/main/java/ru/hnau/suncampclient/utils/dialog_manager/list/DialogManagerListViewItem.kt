package ru.hnau.suncampclient.utils.dialog_manager.list

import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.jutils.possible.Possible


data class DialogManagerListViewItem(
        val icon: DrawableGetter? = null,
        val title: StringGetter,
        val subtitle: StringGetter? = null,
        val action: () -> Unit
)