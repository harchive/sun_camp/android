package ru.hnau.suncampclient.utils.dialog_manager.list

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.view.list.base.BaseList
import ru.hnau.androidutils.ui.view.list.base.BaseListItemsDivider
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.suncampclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class DialogManagerListView(
        context: Context,
        items: List<DialogManagerListViewItem>
) : BaseList<DialogManagerListViewItem>(
        context = context,
        viewWrappersCreator = { DialogManagerListViewItemView(context) },
        orientation = BaseListOrientation.VERTICAL,
        itemsProducer = DataProducer(items),
        fixedSize = true
)