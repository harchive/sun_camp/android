package ru.hnau.suncampclient

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDexApplication
import ru.hnau.androidutils.utils.ContextConnector


class App : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        ContextConnector.init(base)
    }

}