package ru.hnau.suncampclient.ui.view.input

import android.text.InputType


enum class InputSymbolsType(
        val typeValue: Int
) {
    TEXT(InputType.TYPE_CLASS_TEXT),
    NUMBERS(InputType.TYPE_CLASS_NUMBER)
}