package ru.hnau.suncampclient.ui.view.qr_code

import android.content.Context
import android.graphics.*
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.drawables.DrawableOpacity
import ru.hnau.androidutils.ui.drawables.HDrawable
import ru.hnau.jutils.getter.Getter
import kotlin.math.min


class QrCodeDrawable(
        context: Context,
        initialCode: String? = null
) : HDrawable() {

    companion object {
        private val ENCODER = MultiFormatWriter()
        private val PREFERRED_SIZE = DpPxGetter.fromDp(256)
        private val ENCODE_HITS = mapOf(EncodeHintType.ERROR_CORRECTION to ErrorCorrectionLevel.L)
    }

    private val preferredSize = PREFERRED_SIZE.getPxInt(context)

    var code: String? = initialCode
        set(value) {
            if (field != value) {
                field = value
                bitmap.clear()
            }
        }

    private val paint = Paint()

    private val bitmap = Getter(null, this::generateBitmap)

    override fun getIntrinsicWidth() = preferredSize

    override fun getIntrinsicHeight() = preferredSize

    private fun generateBitmap(): Bitmap {
        val size = min(bounds.width(), bounds.height())
        val bitMatrix = ENCODER.encode(code, BarcodeFormat.QR_CODE, size, size, ENCODE_HITS)
        val pixels = IntArray(size * size) { pos ->
            val x = pos / size
            val y = pos % size
            val value = bitMatrix[x, y]
            if (value) Color.BLACK else Color.WHITE
        }
        return Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888).apply {
            setPixels(pixels, 0, size, 0, 0, size, size)
        }
    }

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        bitmap.clear()
    }

    override fun draw(canvas: Canvas, width: Float, height: Float) {
        canvas.drawBitmap(bitmap.get(), 0f, 0f, paint)
    }

    override fun getDrawableOpacity() = DrawableOpacity.OPAQUE

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
    }
}