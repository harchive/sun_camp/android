package ru.hnau.suncampclient.ui

import android.app.Activity
import android.os.Bundle
import android.support.v4.view.animation.FastOutSlowInInterpolator
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawableLayoutType
import ru.hnau.androidutils.ui.utils.PermissionsRequester
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.LevelsView
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.androidutils.ui.view.pager.PagePagerConnector
import ru.hnau.androidutils.ui.view.pager.Pager
import ru.hnau.androidutils.ui.view.pager.PagerPagesStack
import ru.hnau.androidutils.ui.view.view_changer.ViewChangerInfo
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.jutils.takeIfNotBlank
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.pages.auth.AuthPage
import ru.hnau.suncampclient.pages.auth.AuthPageInfo
import ru.hnau.suncampclient.pages.login.LoginPage
import ru.hnau.suncampclient.utils.AppActivityConnector
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.LoginManager
import ru.hnau.suncampclient.utils.dialog_manager.DialogManager

class AppActivity : Activity() {

    companion object {

        private val PAGES_STACK = object : PagerPagesStack() {}
        private val PAGER_INTERPOLATOR = FastOutSlowInInterpolator()

    }

    private val pager by lazy {

        Pager(
                context = this,
                initialPageGetter = {
                    LoginManager.login.takeIfNotBlank()?.let { AuthPage(it, AuthPageInfo.FOR_START) }
                            ?: LoginPage()
                },
                pagesStack = PAGES_STACK,
                viewChangerInfo = ViewChangerInfo(
                        viewDecorationDrawer = null,
                        showInterpolator = PAGER_INTERPOLATOR,
                        hideInterpolator = PAGER_INTERPOLATOR
                )
        ).apply {

            background = LayoutDrawable(
                    context = this@AppActivity,
                    content = DrawableGetter(R.drawable.sunflowers_dark),
                    layoutType = LayoutDrawableLayoutType.PROPORTIONAL_OUT,
                    gravity = HGravity.CENTER
            )
        }
    }

    val finishersLockedProducer = FinishersLockedProducer()

    private val contentView: LevelsView by lazy {
        LevelsView(
                context = this,
                innerViews = listOf(
                        pager,
                        DialogManager.getView(),
                        ColorManager.createWaiter(this, finishersLockedProducer)
                )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentView)
        resources.getString(R.string.main_page_balance_title)
        AppActivityConnector.onAppActivityCreated(this)
    }

    fun showPage(
            page: Page<*>,
            clearStack: Boolean = false,
            precedingPages: List<Page<*>> = emptyList()
    ) =
            pager.showPage(page, clearStack, precedingPages)

    fun goBack(
            pageResolver: (List<Page<*>>) -> Int? = PagePagerConnector.DEFAULT_GO_BACK_PAGE_RESOLVER
    ) =
            pager.goBack(pageResolver)

    override fun onBackPressed() {
        if (contentView.handleGoBack()) {
            return
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        AppActivityConnector.onAppActivityDestroyed()
        pager.release()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionsRequester.onRequestResult(requestCode, permissions, grantResults)
    }

}
