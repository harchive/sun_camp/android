package ru.hnau.suncampclient.ui.view.sun_button

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.ripple.RippleDrawInfo
import ru.hnau.suncampclient.utils.ColorManager


data class SunButtonColorInfo(
        val textColor: ColorGetter,
        val rippleDrawInfo: RippleDrawInfo
) {

    companion object {

        val BROWN_ON_YELLOW = SunButtonColorInfo(ColorManager.BROWN, ColorManager.YELLOW_RIPPLE_DRAW_INFO)
        val BROWN_ON_ORANGE = SunButtonColorInfo(ColorManager.BROWN, ColorManager.ORANGE_RIPPLE_DRAW_INFO)
        val BROWN_ON_RED = SunButtonColorInfo(ColorManager.BROWN, ColorManager.RED_RIPPLE_DRAW_INFO)

        val WHITE_ON_YELLOW = SunButtonColorInfo(ColorManager.WHITE, ColorManager.YELLOW_RIPPLE_DRAW_INFO)
        val WHITE_ON_ORANGE = SunButtonColorInfo(ColorManager.WHITE, ColorManager.ORANGE_RIPPLE_DRAW_INFO)
        val WHITE_ON_RED = SunButtonColorInfo(ColorManager.WHITE, ColorManager.RED_RIPPLE_DRAW_INFO)

        val YELLOW_ON_WHITE = SunButtonColorInfo(ColorManager.YELLOW, ColorManager.WHITE_RIPPLE_DRAW_INFO)
        val ORANGE_ON_WHITE = SunButtonColorInfo(ColorManager.ORANGE, ColorManager.WHITE_RIPPLE_DRAW_INFO)
        val RED_ON_WHITE = SunButtonColorInfo(ColorManager.RED, ColorManager.WHITE_RIPPLE_DRAW_INFO)
        val BROWN_ON_WHITE = SunButtonColorInfo(ColorManager.BROWN, ColorManager.WHITE_RIPPLE_DRAW_INFO)

    }

}