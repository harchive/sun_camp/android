package ru.hnau.suncampclient.ui.view.input

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.text.InputFilter
import android.widget.EditText
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.drawing.ColorUtils
import ru.hnau.androidutils.ui.utils.drawing.doInState
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawer
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawerPathShape
import ru.hnau.androidutils.ui.utils.shadow.info.ShadowInfo
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.initWithRoundSidesRect


@SuppressLint("ViewConstructor")
class Input(
        context: Context,
        initialText: String = "",
        hintText: StringGetter? = null,
        private val info: InputInfo = InputInfo(),
        shadowInfo: ShadowInfo = ShadowInfo.DEFAULT
) : EditText(
        context
) {

    private val contentRect = RectF()
    private val contentPath = Path()

    private val shadowDrawer = ShadowDrawer(context, shadowInfo, ShadowDrawerPathShape(contentPath))

    private val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = info.backgroundColor.get(context)
    }

    init {
        textSize = info.textSize.getDp(context)
        setTextColor(info.textColor.get(context))
        setHintTextColor(info.textColor.get(context).let { ColorUtils.colorWithAlpha(it, 0.5f) })
        hintText?.let { hint = it.get(context) }
        maxLines = 1
        minLines = 1
        inputType = info.symbolsType.typeValue
        info.imeAction.actionValue?.let { imeOptions = it }
        updatePadding()
        info.maxLength?.let { filters += InputFilter.LengthFilter(it) }
        setText(initialText)
        background = null
        setSoftwareRendering()
        includeFontPadding = false
        typeface = FontManager.PT_SANS.get(context).typeface
    }

    override fun draw(canvas: Canvas) {
        canvas.doInState {
            translate(scrollX.toFloat(), scrollY.toFloat())
            shadowDrawer.draw(canvas)
            canvas.drawPath(contentPath, backgroundPaint)
        }
        super.draw(canvas)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        contentRect.set(
                shadowDrawer.shadowSizeRect.left,
                shadowDrawer.shadowSizeRect.top,
                width - shadowDrawer.shadowSizeRect.right,
                height - shadowDrawer.shadowSizeRect.bottom
        )

        contentPath.initWithRoundSidesRect(contentRect)
    }

    private fun updatePadding() {
        setPadding(
                info.paddingHorizontal.getPx(context) + shadowDrawer.shadowSizeRect.left,
                info.paddingVertical.getPx(context) + shadowDrawer.shadowSizeRect.top,
                info.paddingHorizontal.getPx(context) + shadowDrawer.shadowSizeRect.right,
                info.paddingVertical.getPx(context) + shadowDrawer.shadowSizeRect.bottom
        )
    }

}