package ru.hnau.suncampclient.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.view.MotionEvent
import android.widget.FrameLayout
import ru.hnau.androidutils.ui.ripple.RippleDrawInfo
import ru.hnau.androidutils.ui.ripple.RippleDrawer
import ru.hnau.androidutils.ui.ripple.RippleDrawerPathShapeInfo
import ru.hnau.androidutils.ui.utils.shadow.drawer.ButtonShadowDrawer
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawerPathShape
import ru.hnau.androidutils.ui.utils.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.initWithRoundSidesRect


@SuppressLint("ViewConstructor")
open class SunClickableFrameLayout(
        context: Context,
        rippleDrawInfo: RippleDrawInfo = ColorManager.WHITE_RIPPLE_DRAW_INFO,
        shadowInfo: ButtonShadowInfo = ColorManager.BUTTON_SHADOW_INFO
) : FrameLayout(context) {

    private val contentRect = RectF()
    private val contentPath = Path()

    private val rippleDrawer = RippleDrawer(
            context = context,
            rippleDrawInfo = rippleDrawInfo,
            shapeInfo = RippleDrawerPathShapeInfo(contentPath)
    )

    private val shadowDrawer = ButtonShadowDrawer(
            context = context,
            shadowInfo = shadowInfo,
            shape = ShadowDrawerPathShape(contentPath)
    )

    init {
        setSoftwareRendering()
        setSuperPadding(0, 0, 0, 0)
    }

    override fun setPadding(left: Int, top: Int, right: Int, bottom: Int) =
            setSuperPadding(left, top, right, bottom)

    private fun setSuperPadding(left: Int, top: Int, right: Int, bottom: Int) = super.setPadding(
            left + shadowDrawer.shadowSizeRect.left.toInt(),
            top + shadowDrawer.shadowSizeRect.top.toInt(),
            right + shadowDrawer.shadowSizeRect.right.toInt(),
            bottom + shadowDrawer.shadowSizeRect.bottom.toInt()
    )

    override fun dispatchDraw(canvas: Canvas) {
        shadowDrawer.draw(canvas)
        rippleDrawer.draw(canvas)
        super.dispatchDraw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (event.action == MotionEvent.ACTION_DOWN) {
            if (isEventInButton(event)) {
                shadowDrawer.animateToPressed()
            } else {
                return false
            }
        }

        if (event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL) {
            shadowDrawer.animateToNormal()
        }

        if (event.action == MotionEvent.ACTION_UP && isEventInButton(event)) {
            onClicked()
        }

        rippleDrawer.handleMotionEvent(event)

        return true
    }

    protected open fun onClicked() {}

    private fun isEventInButton(event: MotionEvent) =
            contentRect.contains(event.x, event.y)

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        contentRect.set(
                shadowDrawer.shadowSizeRect.left,
                shadowDrawer.shadowSizeRect.top,
                width - shadowDrawer.shadowSizeRect.right,
                height - shadowDrawer.shadowSizeRect.bottom
        )

        contentPath.initWithRoundSidesRect(contentRect)


    }

    private fun onNeedInvalidate(param: Unit) {
        invalidate()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        rippleDrawer.attach(this::onNeedInvalidate)
        shadowDrawer.attach(this::onNeedInvalidate)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        rippleDrawer.detach(this::onNeedInvalidate)
        shadowDrawer.detach(this::onNeedInvalidate)
    }

}