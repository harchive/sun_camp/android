package ru.hnau.suncampclient.ui.view.inscribe_drawable_view


enum class InscribeDrawableViewMaximizationType {

    HORIZONTAL,
    VERTICAL

}