package ru.hnau.suncampclient.ui.view.sun_button

import ru.hnau.androidutils.context_getters.DpPxGetter


data class SunButtonSizeInfo(
        val fontSize: DpPxGetter,
        val paddingHorizontal: DpPxGetter,
        val paddingVertical: DpPxGetter
) {

    companion object {

        val SIZE_12 = SunButtonSizeInfo(DpPxGetter.fromDp(12), DpPxGetter.fromDp(8), DpPxGetter.fromDp(4))
        val SIZE_16 = SunButtonSizeInfo(DpPxGetter.fromDp(16), DpPxGetter.fromDp(12), DpPxGetter.fromDp(8))
        val SIZE_24 = SunButtonSizeInfo(DpPxGetter.fromDp(24), DpPxGetter.fromDp(16), DpPxGetter.fromDp(12))

    }

}