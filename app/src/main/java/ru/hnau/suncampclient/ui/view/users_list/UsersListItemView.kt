package ru.hnau.suncampclient.ui.view.users_list

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLeftMargin
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.suncamp.common.data.User
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.ui.view.SunClickableFrameLayout
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.SizeManager


@SuppressLint("ViewConstructor")
class UsersListItemView(
        context: Context,
        private val onClicked: (User) -> Unit
) : SunClickableFrameLayout(
        context
), BaseListViewWrapper<User> {

    override val view: View
        get() = this

    private val iconView = ImageView(context).apply {
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
    }

    private val nameView = Label(
            context = context,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(20),
            gravity = HGravity.LEFT_CENTER_VERTICAL,
            fontType = FontManager.PT_SANS,
            textColor = ColorManager.BROWN
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f) {
            setLeftMargin(SizeManager.DEFAULT_PADDING.get(context))
        }
    }

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER
        addView(iconView)
        addView(nameView)
    }

    private var user: User? = null

    init {
        addView(contentContainer)
        setPadding(SizeManager.DEFAULT_PADDING, SizeManager.DEFAULT_PADDING)
    }

    override fun setContent(content: User) {
        this.user = content
        nameView.text = StringGetter(content.name)
        iconView.setImageResource(if (content.type == UserType.storage) R.drawable.ic_storage else R.drawable.ic_user)
    }

    override fun onClicked() {
        super.onClicked()
        val user = user ?: return
        onClicked.invoke(user)
    }


}