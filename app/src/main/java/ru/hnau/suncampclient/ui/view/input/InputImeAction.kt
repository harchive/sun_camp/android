package ru.hnau.suncampclient.ui.view.input

import android.view.inputmethod.EditorInfo


enum class InputImeAction(
        val actionValue: Int?
) {
    NONE(null),
    NEXT(EditorInfo.IME_ACTION_NEXT),
    DONE(EditorInfo.IME_ACTION_DONE)
}