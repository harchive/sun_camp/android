package ru.hnau.suncampclient.ui.view.inscribe_drawable_view

import android.content.Context
import android.graphics.Canvas
import android.view.View
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.androidutils.ui.view.utils.horizontalPaddingSum
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum
import ru.hnau.jutils.takeIfPositive


class InscribeDrawableView(
        context: Context,
        drawable: DrawableGetter,
        private val maximizationType: InscribeDrawableViewMaximizationType
) : View(context) {


    private val drawable = drawable.get(context)

    private val drawableAspectRatio = this.drawable.intrinsicHeight.takeIfPositive()?.toFloat()?.let { height ->
        val width = this.drawable.intrinsicWidth.takeIfPositive()?.toFloat() ?: return@let null
        width / height
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        drawable.draw(canvas)
    }

    override fun layout(l: Int, t: Int, r: Int, b: Int) {
        super.layout(l, t, r, b)
        drawable.setBounds(
                paddingLeft,
                paddingTop,
                width - horizontalPaddingSum,
                height - verticalPaddingSum
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (drawableAspectRatio == null) {
            setMeasuredDimension(horizontalPaddingSum, verticalPaddingSum)
            return
        }
        when (maximizationType) {

            InscribeDrawableViewMaximizationType.HORIZONTAL ->
                measureByWidth(widthMeasureSpec, drawableAspectRatio)

            InscribeDrawableViewMaximizationType.VERTICAL ->
                measureByHeight(heightMeasureSpec, drawableAspectRatio)

        }
    }

    private fun measureByWidth(widthMeasureSpec: Int, drawableAspectRatio: Float) {
        val maxWidth = getMaxMeasurement(widthMeasureSpec, 0)
        val width = maxWidth - horizontalPaddingSum
        val height = (width / drawableAspectRatio).toInt()
        val maxHeight = height + verticalPaddingSum
        setMeasuredDimension(maxWidth, maxHeight)
    }

    private fun measureByHeight(heightMeasureSpec: Int, drawableAspectRatio: Float) {
        val maxHeight = getMaxMeasurement(heightMeasureSpec, 0)
        val height = maxHeight - verticalPaddingSum
        val width = (height * drawableAspectRatio).toInt()
        val maxWidth = width + horizontalPaddingSum
        setMeasuredDimension(maxWidth, maxHeight)

    }

}