package ru.hnau.suncampclient.ui.view.sun_button

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.ripple.RippleDrawer
import ru.hnau.androidutils.ui.ripple.RippleDrawerPathShapeInfo
import ru.hnau.androidutils.ui.utils.drawing.initAsCircle
import ru.hnau.androidutils.ui.utils.shadow.drawer.ButtonShadowDrawer
import ru.hnau.androidutils.ui.utils.shadow.drawer.ShadowDrawerPathShape
import ru.hnau.androidutils.ui.utils.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.horizontalPaddingSum
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum
import ru.hnau.suncampclient.utils.FontManager
import ru.hnau.suncampclient.utils.initWithRoundSidesRect
import kotlin.math.max


@SuppressLint("ViewConstructor")
class SunButton(
        context: Context,
        text: StringGetter,
        private val onClick: () -> Unit,
        size: SunButtonSizeInfo,
        color: SunButtonColorInfo,
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT
) : View(context) {

    private val text = text.get(context).toUpperCase()

    private val paint = FontManager.PT_SANS.get(context).createPaint().apply {
        this.color = color.textColor.get(context)
        textSize = size.fontSize.getPx(context)
        textAlign = Paint.Align.CENTER
    }

    private val preferredWidth = (paint.measureText(this.text) + size.paddingHorizontal.getPx(context) * 2).toInt()
    private val preferredHeight = (-paint.fontMetrics.ascent + paint.fontMetrics.descent + size.paddingVertical.getPx(context) * 2).toInt()

    private val contentRect = RectF()
    private val contentPath = Path()

    private val rippleDrawer = RippleDrawer(
            context = context,
            rippleDrawInfo = color.rippleDrawInfo,
            shapeInfo = RippleDrawerPathShapeInfo(contentPath)
    )

    private val shadowDrawer = ButtonShadowDrawer(
            context = context,
            shadowInfo = shadowInfo,
            shape = ShadowDrawerPathShape(contentPath)
    )

    init {
        setSoftwareRendering()
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        shadowDrawer.draw(canvas)
        rippleDrawer.draw(canvas)

        val textX = contentRect.centerX()
        val textY = contentRect.centerY() + (-paint.fontMetrics.ascent - paint.fontMetrics.descent) / 2
        canvas.drawText(text, textX, textY, paint)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (event.action == MotionEvent.ACTION_DOWN) {
            if (isEventInButton(event)) {
                shadowDrawer.animateToPressed()
            } else {
                return false
            }
        }

        if (event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL) {
            shadowDrawer.animateToNormal()
        }

        if (event.action == MotionEvent.ACTION_UP && isEventInButton(event)) {
            onClick.invoke()
        }

        rippleDrawer.handleMotionEvent(event)

        return true
    }

    private fun isEventInButton(event: MotionEvent) =
            contentRect.contains(event.x, event.y)

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        contentRect.set(
                paddingLeft + shadowDrawer.shadowSizeRect.left,
                paddingTop + shadowDrawer.shadowSizeRect.top,
                width - paddingRight - shadowDrawer.shadowSizeRect.right,
                height - paddingBottom - shadowDrawer.shadowSizeRect.bottom
        )

        contentPath.initWithRoundSidesRect(contentRect)


    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = (preferredWidth + shadowDrawer.shadowSizeRect.left + shadowDrawer.shadowSizeRect.right + horizontalPaddingSum).toInt()
        val height = (preferredHeight + shadowDrawer.shadowSizeRect.top + shadowDrawer.shadowSizeRect.bottom + verticalPaddingSum).toInt()
        setMeasuredDimension(
                getDefaultMeasurement(widthMeasureSpec, width),
                getDefaultMeasurement(heightMeasureSpec, height)
        )
    }

    private fun onNeedInvalidate(param: Unit) {
        invalidate()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        rippleDrawer.attach(this::onNeedInvalidate)
        shadowDrawer.attach(this::onNeedInvalidate)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        rippleDrawer.detach(this::onNeedInvalidate)
        shadowDrawer.detach(this::onNeedInvalidate)
    }

}