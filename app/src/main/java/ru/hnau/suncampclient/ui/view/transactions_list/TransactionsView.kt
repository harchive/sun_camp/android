package ru.hnau.suncampclient.ui.view.transactions_list

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.ContentLoader
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.WrapProducer
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.MyTransactionsManager
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.ui.view.EmptyInfoView
import ru.hnau.suncampclient.ui.view.sun_button.SunButtonColorInfo
import ru.hnau.suncampclient.utils.ColorManager
import ru.hnau.suncampclient.utils.outcome_manager.OutcomeManager


@SuppressLint("ViewConstructor")
class TransactionsView(
        context: Context,
        contentProducer: Producer<Finisher<Possible<List<Transaction>>>>,
        private val relativelyUserLogin: String
) : ContentLoader<Possible<List<Transaction>>>(
        context = context,
        contentProducer = contentProducer,
        fromSide = Side.BOTTOM,
        scrollFactor = 0.25f,
        animationTime = TimeValue.MILLISECOND * 300
) {



    private val errorWhileLoadingView: View by lazy {
        EmptyInfoView.createForLoadingError(
                context = context,
                onNeedReload = MyTransactionsManager::invalidate,
                titleColor = ColorManager.YELLOW,
                buttonColor = SunButtonColorInfo.BROWN_ON_YELLOW
        )
    }

    private val emptyInfoView: View by lazy {
        EmptyInfoView(
                context = context,
                title = StringGetter(R.string.main_page_transaction_empty_title),
                button = StringGetter(R.string.main_page_transaction_empty_button) to OutcomeManager::doOutcome,
                titleColor = ColorManager.YELLOW,
                buttonColor = SunButtonColorInfo.BROWN_ON_YELLOW
        )
    }

    override fun generateWaiterView(lockedProducer: LockedProducer) =
            ColorManager.createWaiter(context, lockedProducer)

    override fun generateContentView(data: Possible<List<Transaction>>) =
            data.handle(
                    onSuccess = {
                        if (it.isEmpty()) {
                            getEmptyView()
                        } else {
                            getList(it)
                        }
                    },
                    onError = { getErrorView() }
            )

    private fun getErrorView() = errorWhileLoadingView

    private fun getEmptyView() = emptyInfoView

    private fun getList(items: List<Transaction>) =
            TransactionsListView(context, items, relativelyUserLogin)

    fun updateData() {
        UsersManager.invalidate()
        MyTransactionsManager.invalidate()
    }

}