package ru.hnau.suncampclient.ui.view.transactions_list

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.dp_px.DpPx
import ru.hnau.androidutils.ui.dp_px.dpToPx
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.finisher.await
import ru.hnau.suncamp.common.data.Transaction
import ru.hnau.suncampclient.R
import ru.hnau.suncampclient.data.UsersManager
import ru.hnau.suncampclient.pages.transaction_info.TransactionInfoPage
import ru.hnau.suncampclient.ui.view.SunClickableFrameLayout
import ru.hnau.suncampclient.utils.*
import ru.hnau.suncampclient.utils.timestamp.TimestampType


@SuppressLint("ViewConstructor")
class TransactionViewWrapper(
        context: Context,
        private val relativelyUserLogin: String
) : SunClickableFrameLayout(
        context = context,
        shadowInfo = ColorManager.BUTTON_SHADOW_INFO,
        rippleDrawInfo = ColorManager.WHITE_RIPPLE_DRAW_INFO
), BaseListViewWrapper<Transaction> {

    override val view: View
        get() = this

    private val timestampView = Label(
            context = context,
            fontType = FontManager.PT_SANS,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(16),
            textColor = ColorManager.BROWN,
            gravity = HGravity.LEFT_CENTER_VERTICAL
    ).apply {
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            setBottomMargin(dpToPx(context, 2))
        }
    }

    private val fromUserView = Label(
            context = context,
            fontType = FontManager.PT_SANS,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(16),
            textColor = ColorManager.BROWN,
            gravity = HGravity.LEFT_CENTER_VERTICAL
    )

    private val toUserView = Label(
            context = context,
            fontType = FontManager.PT_SANS,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(16),
            textColor = ColorManager.BROWN,
            gravity = HGravity.LEFT_CENTER_VERTICAL
    )

    private val fromToContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER_VERTICAL

        val fromToIconView = ImageView(context).apply {
            setImageResource(R.drawable.ic_from_to)
            setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
                val horizontalMargin = DpPx.fromDp(context, 4).pxInt
                setMargins(horizontalMargin, 0, horizontalMargin, 0)
            }
        }

        addView(fromUserView)
        addView(fromToIconView)
        addView(toUserView)
    }.apply {
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            setTopMargin(dpToPx(context, 2))
        }
    }

    private val titlesContainer = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL
        gravity = Gravity.CENTER_VERTICAL or Gravity.LEFT
        addView(timestampView)
        addView(fromToContainer)

        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f) {
            setRightMargin(dpToPx(context, 8))
        }
    }

    private val amountView = Label(
            context = context,
            fontType = FontManager.PT_SANS,
            maxLines = 1,
            minLines = 1,
            textSize = DpPxGetter.fromDp(36),
            gravity = HGravity.RIGHT_CENTER_VERTICAL
    ).apply {
        setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
            gravity = Gravity.CENTER_VERTICAL
        }
    }

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER
        setFrameLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT) {
            setMargins(SizeManager.DEFAULT_PADDING.get(context))
        }
        addView(titlesContainer)
        addView(amountView)
    }

    private var transaction: Transaction? = null

    init {
        addView(contentContainer)
    }

    override fun onClicked() {
        super.onClicked()
        val transaction = this.transaction ?: return
        AppActivityConnector.showPage(TransactionInfoPage(transaction))
    }

    override fun setContent(content: Transaction) {

        this.transaction = content

        val income = content.toUserLogin == relativelyUserLogin
        val amountPrefix = if (income) "+" else "-"
        amountView.text = (amountPrefix + content.amount).toGetter()
        amountView.textColor = if (income) ColorManager.BROWN else ColorManager.RED

        timestampView.text = TimestampType.formatTimestamp(content.timestamp).toGetter()

        fillUserName(fromUserView, content.fromUserLogin)
        fillUserName(toUserView, content.toUserLogin)

    }

    private fun fillUserName(
            view: Label,
            login: String
    ) {
        if (login == relativelyUserLogin) {
            view.text = "Я".toGetter()
            return
        }
        UsersManager.getByLogin(login).await(
                onSuccess = {
                    view.text = it.name.toGetter()
                },
                onError = {
                    view.text = "<Ошибка>".toGetter()
                }
        )
    }


}